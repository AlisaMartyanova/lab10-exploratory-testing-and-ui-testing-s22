import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestYandexMusic {

    @Test
    public void testMusicChart(){
        WebDriver driver = new FirefoxDriver();
        driver.get("https://music.yandex.ru/home");

        // close advertisement
        WebElement close = driver.findElement(By.xpath("//span[@class='d-icon deco-icon d-icon_cross-big  local-icon-theme-black']"));
        if (close != null){
            close.click();
        }

        // get music chart
        WebElement chart = driver.findElement(By.xpath("//div[@class='page-line page-line_chart ']"));
        System.out.println("Yandex music chart: " + chart.getText());

        Assert.assertNotEquals(chart, null);

        driver.quit();
    }

    @Test
    public void testControlPlayerBar(){
        WebDriver driver = new FirefoxDriver();
        driver.get("https://music.yandex.ru/new-releases");

        // close advertisement
        WebElement close = driver.findElement(By.xpath("//span[@class='d-icon deco-icon d-icon_cross-big  local-icon-theme-black']"));
        if (close != null){
            close.click();
        }

        // click on listen to music button
        WebElement music = driver.findElement(By.xpath("//div[@class='d-hover']"));
        music.click();

        // get music player bar
        WebElement bar = driver.findElement(By.xpath("//div[@class='player-controls deco-player-controls']"));
        System.out.println("Music player bar: " + bar.getText());
        Assert.assertNotEquals(bar, null);

        driver.quit();
    }

    @Test
    public void testListenWithoutLogin(){
        WebDriver driver = new FirefoxDriver();
        driver.get("https://music.yandex.ru/new-releases");

        // close advertisement
        WebElement close = driver.findElement(By.xpath("//span[@class='d-icon deco-icon d-icon_cross-big  local-icon-theme-black']"));
        if (close != null){
            close.click();
        }

        // click on listen to music button
        WebElement music = driver.findElement(By.xpath("//div[@class='d-hover']"));
        music.click();

        // get login request
        WebElement login = driver.findElement(By.xpath("//div[@class='d-tooltip__content deco-tooltip']"));
        System.out.println("Login request: " + login.getText());
        Assert.assertNotEquals(login, null);

        driver.quit();
    }

}
